import pygame
from pygame.locals import *
from random import randint
from operator import itemgetter

WINDOW_SIZE = (1280,720)
window = pygame.display.set_mode(WINDOW_SIZE)

def kopia(list):
    kopia=[]
    for i in list:
        kopia.append(i)
    return kopia

def BubbleSort(list,plik):
    sorted_list=kopia(list)
    for i in range(len(sorted_list)):
        for j in range(len(sorted_list)-i-1):
            if sorted_list[j+1][plik]<sorted_list[j][plik]:
                sorted_list[j], sorted_list[j+1]=sorted_list[j+1], sorted_list[j]
    return sorted_list

def BubbleSort2(list):
    sorted_list=kopia(list)
    for i in range(len(sorted_list)):
        for j in range(len(sorted_list)-i-1):
            if sorted_list[j+1] <sorted_list[j]:
                sorted_list[j], sorted_list[j+1]=sorted_list[j+1], sorted_list[j]
    return sorted_list

def sort_list(list,plik):
    sorted_list=kopia(list)
    sorted_list=sorted(sorted_list, key=itemgetter(plik))
    return sorted_list


def list_reverse(list):
    reversed_list=[]
    for i in range(len(list)):
        reversed_list.append(list[len(list)-i-1])
    return reversed_list

class Nickname():
    def __init__(self,x_cord, y_cord):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.button_image = pygame.image.load("nick_button.png")
        self.hovered_button_image = pygame.image.load("nick_button_hovered.png")
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.button_image.get_width(), self.button_image.get_height())

    def tick(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            return True

    def draw(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            if pygame.mouse.get_pressed()[0]:
                window.blit(self.hovered_button_image, (self.x_cord, self.y_cord))
            else:
                window.blit(self.button_image, (self.x_cord, self.y_cord))
        else:
            window.blit(self.button_image, (self.x_cord, self.y_cord))

class Wroc():
    def __init__(self, x_cord, y_cord):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.button_image = pygame.image.load("back_button.png")
        self.hovered_button_image = pygame.image.load("back_button_hovered.png")
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.button_image.get_width(), self.button_image.get_height())

    def tick(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            if pygame.mouse.get_pressed()[0]:
                return True

    def draw(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            window.blit(self.hovered_button_image, (self.x_cord, self.y_cord))
        else:
            window.blit(self.button_image, (self.x_cord, self.y_cord))

class Instrukcja():
    def __init__(self, x_cord, y_cord):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.button_image = pygame.image.load("instrukcja_button.png")
        self.hovered_button_image = pygame.image.load("instrukcja_button_hovered.png")
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.button_image.get_width(), self.button_image.get_height())

    def tick(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            if pygame.mouse.get_pressed()[0]:
                return True

    def draw(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            window.blit(self.hovered_button_image, (self.x_cord, self.y_cord))
        else:
            window.blit(self.button_image, (self.x_cord, self.y_cord))

class Button():
    def __init__(self, x_cord, y_cord):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.button_image = pygame.image.load("button_projekt.png")
        self.hovered_button_image = pygame.image.load("button_hovered_projekt.png")
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.button_image.get_width(), self.button_image.get_height())

    def tick(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            if pygame.mouse.get_pressed()[0]:
                return True

    def draw(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            window.blit(self.hovered_button_image, (self.x_cord, self.y_cord))
        else:
            window.blit(self.button_image, (self.x_cord, self.y_cord))

class Wyniki():
    def __init__(self, x_cord, y_cord):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.button_image = pygame.image.load("WYNIKI.png")
        self.hovered_button_image = pygame.image.load("WYNIKI_hovered.png")
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.button_image.get_width(), self.button_image.get_height())

    def tick(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            if pygame.mouse.get_pressed()[0]:
                return True

    def draw(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            window.blit(self.hovered_button_image, (self.x_cord, self.y_cord))
        else:
            window.blit(self.button_image, (self.x_cord, self.y_cord))

class Player():
    def __init__(self):
        self.x_cord = 610
        self.y_cord = 290
        self.image = pygame.image.load("gracz.png")
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)

    def tick(self):
        self.image = pygame.image.load("gracz_hit.png")

    def tick2(self):
        self.image = pygame.image.load("gracz.png")
        

    def draw(self):
        window.blit(self.image,(self.x_cord, self.y_cord))

class Shield():
    def __init__(self):
        self.x_cord = -40
        self.y_cord = -40
        self.image = pygame.image.load("shield.png")
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)

    def tick(self, keys):
        if keys[pygame.K_w]:
            self.y_cord = 250
            self.x_cord = 610
        if keys[pygame.K_s]:
            self.y_cord = 330
            self.x_cord = 610
        if keys[pygame.K_a]:
            self.x_cord = 570
            self.y_cord = 290
        if keys[pygame.K_d]:
            self.x_cord = 650
            self.y_cord = 290
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)
                    
    def draw(self):
        window.blit(self.image, (self.x_cord, self.y_cord))

class Enemy_up():
    predkosc:int
    def __init__(self):
        self.x_cord = 613
        self.y_cord =  -80
        self.start_x_cord = 613
        self.start_y_cord =  -80
        self.image = pygame.image.load("enemy_up.png")
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)

    def tick(self):
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)
        self.y_cord += self.predkosc

    def pozZero(self):
        self.x_cord = self.start_x_cord
        self.y_cord = self.start_y_cord   

    def draw(self):
        window.blit(self.image, (self.x_cord, self.y_cord))

class Enemy_down():
    predkosc:int
    def __init__(self):
        self.x_cord = 613
        self.y_cord =  620 
        self.start_x_cord = 613
        self.start_y_cord =  620 
        self.image = pygame.image.load("enemy_down.png")
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)

    def tick(self):
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)
        self.y_cord -= self.predkosc

            
    def pozZero(self):
        self.x_cord = self.start_x_cord
        self.y_cord = self.start_y_cord   

    def draw(self):
        window.blit(self.image, (self.x_cord, self.y_cord))

class Enemy_right():
    predkosc:int
    def __init__(self):
        self.x_cord = 1360
        self.y_cord =  293
        self.start_x_cord = 1360
        self.start_y_cord =  293      
        self.image = pygame.image.load("enemy_right.png")
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)

    def tick(self):
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)
        self.x_cord -= self.predkosc


    def pozZero(self):
        self.x_cord = self.start_x_cord
        self.y_cord = self.start_y_cord   

    def draw(self):
        window.blit(self.image, (self.x_cord, self.y_cord))

class Enemy_left():
    predkosc:int
    def __init__(self):
        self.x_cord = -80
        self.y_cord =  293
        self.start_x_cord = -80
        self.start_y_cord =  293
        self.image = pygame.image.load("enemy_left.png")
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)

    def tick(self):
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)
        self.x_cord += self.predkosc


    def pozZero(self):
        self.x_cord = self.start_x_cord
        self.y_cord = self.start_y_cord   

    def draw(self):
        window.blit(self.image, (self.x_cord, self.y_cord))
