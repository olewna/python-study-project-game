from turtle import backward
import pygame, random
from pygame.locals import *
from fav import *
from test_gra import *
pygame.init()

pygame.display.set_caption('ArrowsTheGame')
ikonka = pygame.image.load("ikonka.jpg")
pygame.display.set_icon(ikonka)  
WINDOW_SIZE= (1280,720)
window = pygame.display.set_mode(WINDOW_SIZE)

def level_one():
    run = True
    clock = 0
    hp = 3
    score = 0
    level = ""
    player = Player()
    shield = Shield()
    enemies2 = []
    enemies = [Enemy_up(), Enemy_down(), Enemy_left(), Enemy_right()]
    background = pygame.image.load("background_tlo_projekt.png")
    scoreboard = pygame.Rect(0,620, 1280, 100)
    while run:
        clock += pygame.time.Clock().tick(60)/1000
        if hp <= 0:
            file=open("scores.txt","a")
            punkty = nick+"\n"+str(score)+"\n"
            file.write(punkty)
            file.close()
            run = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                file=open("scores.txt","a")
                punkty = nick+"\n"+str(score)+"\n"
                file.write(punkty)
                file.close()
                run = False
        keys = pygame.key.get_pressed()

        if score >=0 and score <=9:
            level= "LEVEL 1"
            if clock >=randint(2,4):
                clock = 0
                f = randint(2,3)
                enemies2.append(enemies[f])
                enemies[f].predkosc=randint(12,16)
        if score > 9 and score <=24:
            level = "LEVEL 2"
            if clock >= randint(2,4):
                clock = 0
                f = randint(0,1)
                enemies2.append(enemies[f])
                enemies[f].predkosc = randint(10,14)
        if score > 24 and score <= 39:
            level = "LEVEL 3"
            if clock >= randint(2,4):
                clock = 0
                f = randint(0,3)
                enemies2.append(enemies[f])
                enemies[f].predkosc=randint(12,16)
        if score > 39 and score <= 59:
            level = "LEVEL 4"
            if clock >= randint(1,3):
                clock = 0
                f = randint(0,3)
                enemies2.append(enemies[f])
                enemies[f].predkosc=randint(14,18)
        if score > 59 and score <= 79:
            level = "LEVEL 5"
            if clock >= randint(1,3):
                clock = 0
                f = randint(0,3)
                enemies2.append(enemies[f])
                enemies[f].predkosc=randint(17,20)
        if score > 79 and score <= 99:
            level = "LEVEL 666"
            if clock >= 1:
                clock = 0
                f = randint(0,3)
                enemies2.append(enemies[f])
                enemies[f].predkosc=randint(19,22)
        if score > 99 and score <=149:
            if clock >=1:
                clock = 0
                f = randint(0,3)
                enemies2.append(enemies[f])
                enemies[f].predkosc=randint(21,25)
        if score > 149:
            if clock >= 0.5:
                clock = 0
                f = randint(0,3)
                enemies2.append(enemies[f])
                enemies[f].predkosc=randint(21,25)                

        shield.tick(keys)
        for enemy2 in enemies2:
            enemy2.tick()

        for enemy2 in enemies2:
            if player.hitbox.colliderect(enemy2.hitbox):
                hp -= 1
                enemy2.pozZero()
                enemies2.remove(enemy2)
            elif shield.hitbox.colliderect(enemy2.hitbox):
                score += 1
                enemy2.pozZero()
                enemies2.remove(enemy2)


        window.blit(background, (0,0))
        for enemy2 in enemies2:
            enemy2.draw()
        shield.draw()
        player.draw()
        pygame.draw.rect(window, (151, 165, 250), scoreboard)
        score_image = pygame.font.Font.render(pygame.font.SysFont("externalfont",70), "PUNKTY: "+str(score), True, (73, 66, 250))
        window.blit(score_image, (100, 650))
        hp_image = pygame.font.Font.render(pygame.font.SysFont("externalfont",70), "HP: "+str(hp), True, (73, 66, 250))
        window.blit(hp_image, (1000, 650))
        level_which = pygame.font.Font.render(pygame.font.SysFont("externalfont",70), str(level), True, (73, 66, 250))
        window.blit(level_which, (550,650))

        pygame.display.update()

def leadboards():
    run = True
    clock = 0
    back_button = Wroc(40,550)
    background = pygame.image.load("background_wyniki.png")
    while run:
        clock += pygame.time.Clock().tick(60)/1000
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
        if back_button.tick():
            run = False

        window.blit(background,(0,0))
        back_button.draw()

        file=open("scores.txt","r")
        punkty = []
        top10 = []
        plik = file.readlines()
        dlugosc = len(plik)
        for i in range(dlugosc):
            plik[i] = plik[i].replace("\n","")
        for i in range(0,dlugosc,2):
            punkty.append({"nick":plik[i],"score":int(plik[i+1])})
        punkty = BubbleSort(punkty, "score")
        punkty = list_reverse(punkty)
        for i in range(len(punkty)):
            top10.append(str(i+1)+". "+str(punkty[i]["nick"])+" "+str(punkty[i]["score"])+" "+"pkt.")
        for i in range(min(len(top10),10)):
            top10_image = pygame.font.Font.render(pygame.font.SysFont("externalfont",50), str(top10[i]), True, (255, 255, 255))
            window.blit(top10_image,(330, 50+i*50))
        file.close()

        pygame.display.update()

def instrukcja():
    run = True
    clock = 0
    back_button = Wroc(1040,590)
    background = pygame.image.load("background_instrukcja.png")
    while run:
        clock += pygame.time.Clock().tick(60)/1000
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
        if back_button.tick():
            run = False

        window.blit(background,(0,0))
        back_button.draw()
        pygame.display.update()


def main():
    run = True
    clock = 0
    global nick
    nick = "Unknown"
    biały = (255,255,255)
    jasny_szary = (100,100,100)
    active = False
    kolor = jasny_szary
    background = pygame.image.load("tlo_projekt.png")
    play_button = Button(960,220)
    wyniki_button = Wyniki(100,220)
    nick_button = Nickname(450,444)
    instrukcja_button = Instrukcja(450, 560)
    while run:
        clock += pygame.time.Clock().tick(60)/1000

        if nick_button.tick():
            active = True
            kolor = biały
        else:
            active = False
            kolor = jasny_szary   

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.KEYDOWN:    
                if active == True:
                    if event.key == pygame.K_BACKSPACE:
                        nick = nick[:-1]
                    else:
                        if len(nick)<13:
                            nick += event.unicode


        if play_button.tick():
            level_one()
        if wyniki_button.tick():
            leadboards()
        if instrukcja_button.tick():
            instrukcja()

        window.blit(background,(0,0))
        play_button.draw()
        wyniki_button.draw()
        nick_button.draw()
        instrukcja_button.draw()
        nick_image = pygame.font.Font.render(pygame.font.SysFont("externalfont",69), nick, True, kolor)
        window.blit(nick_image,(455,472))
        pygame.display.update()

if __name__ == "__main__":
    main()