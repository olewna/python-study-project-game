import pytest
from fav import *

lista1 = [{"nick":"gracz1","score":1},
{"nick":"gracz2","score":312},
{"nick":"gracz3","score":0},
{"nick":"gracz4","score":12}]

lista2 = [1,3,5,4,6,7]

def test_buble():
    assert BubbleSort(lista1,"score") == sort_list(lista1,"score")

def test_revlist():
    lista3 = lista2.copy()
    lista3.reverse()
    assert lista3 == list_reverse(lista2)

def test_buble2():
    assert BubbleSort2(lista2) == sorted(lista2)

def test_playerxy():
    assert Player().y_cord == 290
    assert Player().x_cord == 610

def test_enemiesxy():
    assert Enemy_up().start_x_cord == 613
    assert Enemy_up().start_y_cord == -80
    assert Enemy_right().start_x_cord == 1360
    assert Enemy_right().start_y_cord == 293
    assert Enemy_down().start_x_cord == 613
    assert Enemy_down().start_y_cord == 620
    assert Enemy_left().start_x_cord == -80
    assert Enemy_left().start_y_cord == 293